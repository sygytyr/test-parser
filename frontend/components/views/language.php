<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 01.11.18
 * Time: 21:36
 */
?>
<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 29.09.2018
 * Time: 21:15
 */

use yii\helpers\Html;

?>

<div class="langs ease <?=$type?>">
    <?php foreach ($langs as $lang): ?>
        <?php if ($lang->locale == $current) { ?>
            <span class="vertical"><span><?= $lang->short_label ?> <em></em></span></span>
        <?php } ?>
    <?php endforeach; ?>

    <?php foreach ($langs as $lang): ?>
        <?php if ($lang->locale != $current) { ?>
            <?= Html::a('<span>' . $lang->short_label . '</span>', '/' . $lang->locale, ['class' => 'vertical']) ?>
        <?php } ?>
    <?php endforeach; ?>
</div>
