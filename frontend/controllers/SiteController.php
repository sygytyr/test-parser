<?php

namespace frontend\controllers;

use backend\modules\article\models\Article;
use backend\modules\article\models\ArticleSearch;
use backend\modules\article\models\Review;
use backend\modules\landing\models\News;
use backend\modules\landing\models\SecurityItem;
use backend\modules\landing\models\Team;
use backend\modules\landing\models\YoutubeVideo;
use backend\modules\service\models\Service;
use backend\modules\service\models\ServiceCategory;
use common\components\Yiit;
use frontend\components\FooterClass;
use frontend\components\HeaderClass;
use frontend\models\CalcBaseModel;
use frontend\models\CallBackForm;
use frontend\models\FurnitureStruct;
use frontend\models\MainCalc;
use frontend\models\MeetingRoomStruct;
use frontend\models\PostCalcModelView;
use Yii;
use yii\base\InvalidParamException;
use yii\data\Pagination;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use yii\web\Response;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

     /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('index', []);
    }



    public function actionAbout()
    {

        return $this->render('about');
    }


    public function actionContact()
    {

        return $this->render('contact');
    }


}
