<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 28.10.18
 * Time: 9:15
 */

namespace common\behaviors;


use backend\models\Language;
use yii\base\Behavior;
use yii\db\ActiveRecord;

class TranslateBehavior extends Behavior
{
    public $owner;
    public $model_id = 'id';

    public function events()
    {
        return [
            //ActiveRecord::EVENT_AFTER_FIND => 'loadData',
            ActiveRecord::EVENT_AFTER_INSERT => 'afterSave',
            ActiveRecord::EVENT_AFTER_UPDATE => 'afterSave',
        ];
    }

    /**
     *
     */
    public function afterSave()
    {
        if (is_null($this->owner->langModel)) {
            return;
        }
        $this->owner->langModel::deleteAll([
            'model_id' => $this->owner->{$this->model_id},
            'language' => Language::getEditLang()
        ]);
        $model = new $this->owner->langModel;
        $model->load(\Yii::$app->request->post());
        $model->model_id = $this->owner->{$this->model_id};
        $model->language = Language::getEditLang();
        $model->save();

    }
}