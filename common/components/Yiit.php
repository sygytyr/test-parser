<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 22.10.18
 * Time: 21:43
 */

namespace common\components;

use common\models\Message;
use common\models\SourceMessage;

class Yiit
{
    public static function tr($category, $translate)
    {
        if(!$translate)
        {
           return '';
        }
        $tranlatedString = md5($category . $translate);
        $cacheGet = \Yii::$app->cache->get(\Yii::$app->language . $tranlatedString);
        if (!$cacheGet) {
            $check = SourceMessage::findOne(['category' => $category, 'message' => $translate]);
            if (!!$check) {
                if ($check->messageLang) {
                    \Yii::$app->cache->set(\Yii::$app->language . $tranlatedString, $check->messageLang->translation ?? false,
                        60 * 60 * 24);
                    return $check->messageLang->translation ?? $translate;
                }

                $modelMessage = new Message();
                $modelMessage->id = $check->id;
                $modelMessage->language = \Yii::$app->language;
                $modelMessage->translation = $translate;
                $modelMessage->save();
                \Yii::$app->cache->set(\Yii::$app->language . $tranlatedString, $translate, 60 * 60 * 24);
                return $translate;

            }
            $modelSourceMessage = new SourceMessage();
            $modelSourceMessage->category = $category;
            $modelSourceMessage->message = $translate;
            $modelSourceMessage->save();

            $modelMessage = new Message();
            $modelMessage->id = $modelSourceMessage->id;
            $modelMessage->language = \Yii::$app->language;
            $modelMessage->translation = $translate;
            $modelMessage->save();
            \Yii::$app->cache->set(\Yii::$app->language . $tranlatedString, $translate, 60 * 60 * 24);
            return $translate;


        }
        return $cacheGet;
    }
}