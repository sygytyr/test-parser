<?php

use common\components\Yiit;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\service\models\ServiceCategory */

$this->title = Yiit::tr('back/base', 'Update') . ' '. $model->getTitle() . ': '. ($model->editLang->label ?? $model->id);
$this->params['breadcrumbs'][] = ['label' => $model->getTitle(), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => ($model->editLang->label ?? $model->id), 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yiit::tr('back/base', 'Update');
?>
<div class="service-category-update">

    <h1><?= Html::encode($this->title) ?></h1>
    <?= $this->render($viewPathModule.'_form', [
        'model' => $model,
    ]) ?>

</div>
