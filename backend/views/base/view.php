<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model \common\models\base\BaseModel */

$this->title = $model->getTitle();
$this->params['breadcrumbs'][] = ['label' => $model->getTitle(), 'url' => ['index']];
?>
<div class="settings-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => $model->viewData()
    ]) ?>

</div>
