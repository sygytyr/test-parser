<?php
/**
 * Created by PhpStorm.
 * User: viktor
 * Date: 05.11.18
 * Time: 16:16
 */

use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $canCreate boolean
 * @var $searchModel \common\models\base\BaseModel
 * @var $dataProvider \yii\data\ActiveDataProvider
 * */

$this->title = $searchModel->getTitle();
$this->params['breadcrumbs'][] = $this->title;


?>
<div class="">
    <h1><?= $this->title ?></h1>
    <?php if ($canCreate) { ?>
        <p>
            <?= Html::a(\common\components\Yiit::tr('back/base', 'Create'), ['create'],
                ['class' => 'btn btn-success']) ?>
        </p>
    <?php } ?>
    <?= GridView::widget(\yii\helpers\ArrayHelper::merge([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $searchModel->getIndexConfig()
    ], $searchModel->additionalOptions())) ?>


</div>


