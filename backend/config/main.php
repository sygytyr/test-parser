<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'advion-back',
    'language' => 'en',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log'],
    'modules' => [
        'rbac' => [
            'class' => 'backend\modules\rbac\Module',
            'layout' => 'left-menu',
            'mainLayout' => '@backend/views/layouts/main.php',
            'controllerMap' => [
                'assignment' => [
                    'class' => 'backend\modules\rbac\controllers\AssignmentController',

                    'idField' => 'id',
                    'usernameField' => 'username',

                ],
            ],
        ],
        'service' => [
            'class' => 'backend\modules\service\ServiceModule',
        ],
        'article' => [
            'class' => 'backend\modules\article\ArticleModule',
        ],
        'static-page' => [
            'class' => 'backend\modules\staticPage\StaticModule',
        ],
        'menu' => [
            'class' => 'backend\modules\menu\MenuModule',
        ],
        'request' => [
            'class' => 'backend\modules\request\RequestModule',
        ],
        'treemanager' =>  [
            'class' => '\kartik\tree\Module',
        ],
        'db-manager' => [
            'class' => 'bs\dbManager\Module',
            'path' => '@backend/web/backups',
            'dbList' => ['db'],
            'as access' => [
                'class' => 'yii\filters\AccessControl',
                'rules' => [
                    [
                        'allow' => true,
                    ],
                ],
            ],
        ],
        'imagemanager' => [
            'class' => 'noam148\imagemanager\Module',
            //set accces rules ()
            'canUploadImage' => true,
            'canRemoveImage' => function () {
                return true;
            },
            'deleteOriginalAfterEdit' => false,
            // false: keep original image after edit. true: delete original image after edit
            // Set if blameable behavior is used, if it is, callable function can also be used
            'setBlameableBehavior' => false,
            //add css files (to use in media manage selector iframe)
            'cssFiles' => [
                'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css',
            ],
        ],
        'gridview' => [
            'class' => '\kartik\grid\Module'
        ],
    ],
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-backend',

        ],
        /*'twitter' => [
            'class' => 'richweber\twitter\Twitter',
            'consumer_key' => 'YOUR_TWITTER_CONSUMER_KEY',
            'consumer_secret' => 'YOUR_TWITTER_CONSUMER_SECRET',
            'callback' => Yii::$app->request->hostInfo. '/landing/medium/twitter-callback',
        ],*/
        'feed' => [
            'class' => 'yii\feed\FeedDriver',
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager', // or use 'yii\rbac\DbManager'
        ],

        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-backend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the backend
            'name' => 'advanced-backend',
        ],
        'imagemanager' => [
            'class' => 'noam148\imagemanager\components\ImageManagerGetPath',
            //set media path (outside the web folder is possible)
            'mediaPath' => 'uploads',
            //path relative web folder. In case of multiple environments (frontend, backend) add more paths
            'cachePath' => ['assets/images', '../../frontend/web/assets/images'],
            //use filename (seo friendly) for resized images else use a hash
            'useFilename' => true,
            //show full url (for example in case of a API)
            'absoluteUrl' => true,
            'databaseComponent' => 'db'
            // The used database component by the image manager, this defaults to the Yii::$app->db component
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
            'cachePath' => '@frontend/runtime/cache'
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],

    ],
    'as access' => [
        'class' => 'backend\modules\rbac\components\AccessControl',
        'allowActions' => [
            'site/login'
        ]
    ],
    'controllerMap' => [
        'elfinder' => [
            'class' => 'mihaildev\elfinder\Controller',
            'access' => ['@'], //глобальный доступ к фаил менеджеру @ - для авторизорованных , ? - для гостей , чтоб открыть всем ['@', '?']
            'disabledCommands' => ['netmount'], //отключение ненужных команд https://github.com/Studio-42/elFinder/wiki/Client-configuration-options#commands
            'roots' => [
                [
                    'baseUrl'=>'@web',
                    'basePath'=>'@frontendweb',
                    'path' => 'uploads',
                    'name' => 'Global'
                ],
            ],
        ]
    ],
    'params' => $params,
];
