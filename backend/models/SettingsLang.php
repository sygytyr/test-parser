<?php
/**
 * Created by PhpStorm.
 * User: viktor
 * Date: 05.11.18
 * Time: 16:07
 */

namespace backend\models;


class SettingsLang extends \common\models\SettingsLang
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['model_id', 'language', 'label'], 'required'],
            [['model_id', 'label'], 'string', 'max' => 255],
            [['language'], 'string', 'max' => 4],
            [['model_id', 'language'], 'unique', 'targetAttribute' => ['model_id', 'language']],
            [
                ['model_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Settings::className(),
                'targetAttribute' => ['model_id' => 'key']
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'model_id' => 'Model ID',
            'language' => 'Language',
            'label' => 'Label',
        ];
    }
}