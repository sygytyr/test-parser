<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "language".
 *
 * @property int $id
 * @property string $label
 * @property string $short_label
 * @property string $locale
 * @property int $default
 * @property int $active
 * @property int $sort_order
 */
class Language extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'language';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['label', 'short_label', 'locale'], 'required'],
            [['default', 'active', 'sort_order'], 'integer'],
            [['label'], 'string', 'max' => 255],
            [['short_label'], 'string', 'max' => 10],
            [['locale'], 'string', 'max' => 20],
            [['active'], 'default', 'value' => 1],
            [['sort_order'], 'default', 'value' => 1],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'label' => 'Label',
            'short_label' => 'Short Label',
            'locale' => 'Locale',
            'default' => 'Default',
            'active' => 'Active',
            'sort_order' => 'Sort Order',
        ];
    }

    public static function getLanguages()
    {
        return self::find()->select('locale')->indexBy('id')->column();
    }

    public static function getEditLang()
    {

        return !!Yii::$app->request->get('lang_id') ? Yii::$app->request->get('lang_id') : self::getDefaultLang()->locale;
    }


    public static $current = null;

    public static function getCurrent()
    {
        return Yii::$app->language;
    }

//Установка текущего объекта языка и локаль пользователя
    public static function setCurrent($url = null)
    {

        $language = self::getLangByUrl($url);

        self::$current = ($language === null) ? self::getDefaultLang() : $language;

        Yii::$app->language = self::$current->locale;
        Yii::$app->sourceLanguage = self::$current->locale;
        Yii::$app->formatter->locale = self::$current->locale;
    }

//Получения объекта языка по умолчанию
    public static function getDefaultLang()
    {
        $defaultLang = Yii::$app->cache->get('defaultlang');
        if (!$defaultLang) {
            $defaultLang = self::find()->where('`default` = :default', [':default' => 1])->one();
            Yii::$app->cache->set('defaultlang', $defaultLang, 60 * 60 * 24);
        }
        return $defaultLang;
    }


//Получения объекта языка по буквенному идентификатору
    public static function getLangByUrl($url = null)
    {

        if ($url === null) {
            return null;
        } else {
            $language = Yii::$app->cache->get('lang' . $url);

            if (!$language) {
                $language = self::find()->where('locale = :locale', [':locale' => $url])->one();
                Yii::$app->cache->set('lang' . $url, $language, 60 * 60 * 24);
            }

            if ($language === null) {
                return null;
            } else {
                return $language;
            }
        }
    }

    public static function getList()
    {
        return self::find()->select('locale')->indexBy('locale')->where(['active' => 1])->orderBy([
            'sort_order' => SORT_DESC,
            'id' => SORT_ASC
        ])->column();
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        Yii::$app->cache->delete('langall');

    }
}
