<?php

namespace backend\models;

use common\components\Yiit;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "test".
 *
 * @property int $id
 * @property int $internal_id
 * @property int $last_modify
 * @property string $regulator
 * @property int $created_at
 * @property int $updated_at
 */
class Test extends \common\models\base\BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'test';
    }

    public function getTitle()
    {
        return Yiit::tr('back/base', 'Test list grid');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['internal_id', 'regulator'], 'required'],
            [['internal_id', 'last_modify', 'created_at', 'updated_at'], 'integer'],
            [['regulator'], 'string', 'max' => 255],
            [['id'], 'string'],
            [['id'], 'unique'],
        ];
    }

    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'timestamp' => [
                'class' => TimestampBehavior::class,
            ],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yiit::tr('back/test', 'ID'),
            'internal_id' => Yiit::tr('back/test', 'Internal ID'),
            'last_modify' => Yiit::tr('back/test', 'Last Modify'),
            'regulator' => Yiit::tr('back/test', 'Regulator'),
            'created_at' => Yiit::tr('back/test', 'Created At'),
            'updated_at' => Yiit::tr('back/test', 'Updated At'),
        ];
    }

    public function getIndexConfig()
    {
        return [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            [
                'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'internal_id',
                'editableOptions' =>
                    [
                        'formOptions' => ['action' => ['editable']],

                    ]
            ],
            [
                'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'regulator',
                'editableOptions' =>
                    [
                        'formOptions' => ['action' => ['editable']],

                    ]
            ],
            [
                'attribute'=>'last_modify',
                'value'=>function($model) {
                    return Yii::$app->formatter->asDatetime($model->last_modify, 'd-MM-Y HH:i');
                },
                'filter'=>false,
            ],

            ['class' => 'yii\grid\ActionColumn', 'template' => '{delete}'],

        ];
    }
}
