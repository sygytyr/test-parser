<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 28.10.18
 * Time: 9:34
 */

namespace backend\components;

use Yii;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class BaseController extends Controller
{
    public $model;
    public $modelSearch;
    //public $viewPath = '@backend/views/base/';
    public $canCreateModel = true;
    public $canDelete = true;
    public $canUpdate = true;
    public $viewPathModule = '';
    public $bathViewPathModule = '@backend/views/base/';

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new $this->modelSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render($this->bathViewPathModule.'index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'canCreate' => $this->canCreateModel
        ]);
    }

    /**
     * Displays a single SiteMenu model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->redirect(['index']);
    }

    /**
     * @return string|\yii\web\Response
     * @throws ForbiddenHttpException
     */
    public function actionCreate()
    {
        if (!$this->canCreateModel) {
            throw new ForbiddenHttpException('You not allowed to create');
        }
        $model = new $this->model;
        $model->loadDefaultValues();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render($this->bathViewPathModule.'create', [
            'model' => $model,
            'viewPathModule' => $this->viewPathModule
        ]);
    }

    /**
     * @param $id
     * @return string|\yii\web\Response
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        if (!$this->canUpdate) {
            throw new ForbiddenHttpException('You not allowed to update');
        }

        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render($this->bathViewPathModule.'update', [
            'model' => $model,
            'viewPathModule' => $this->viewPathModule
        ]);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     */
    public function actionDelete($id)
    {

        if (!$this->canDelete) {
            throw new ForbiddenHttpException('You not allowed to update');
        }

        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }


    /**
     * @param $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        if (($model = $this->model::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionEditable()
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        if (\Yii::$app->request->isPost && !!\Yii::$app->request->post('editableKey') && !!\Yii::$app->request->post('editableAttribute')) {
            $postData = \Yii::$app->request->post();
            $searchModel = $this->modelSearch;
            $searchName = new \ReflectionClass($searchModel);
            $searchName = $searchName->getShortName();
            $attribute = $postData['editableAttribute'];
            $this->model = $this->findModel(\Yii::$app->request->post('editableKey'));
            $this->model->load($postData[$searchName][Yii::$app->request->post('editableIndex')] ?? [], '');

            if ($this->model->validate() && $this->model->save()) {
                $this->model->save();
                return ['output' => $this->model->{$attribute}, 'message' => ''];
            } else {
                return ['output' => $this->model->{$attribute}, 'message' => $this->model->getFirstError($attribute)];
            }
        }
    }
}
