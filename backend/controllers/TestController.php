<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 15.07.2019
 * Time: 23:18
 */

namespace backend\controllers;


use backend\models\Test;
use backend\models\TestSearch;
use backend\components\BaseController;

class TestController extends BaseController
{
    public $model = Test::class;
    public $modelSearch = TestSearch::class;
    public $canCreateModel = false;
    public $canDelete = true;
    public $canUpdate = false;
}