<?php

use yii\db\Migration;

/**
 * Class m181105_135139_insert_default_language
 */
class m181105_135139_insert_default_language extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->batchInsert('language', ['short_label', 'label', 'locale', 'default', 'active', 'sort_order'], [
            [
                'Eng',
                'English',
                'en',
                1,
                1,
                2
            ],
            [
                'Ru',
                'Russian',
                'ru',
                0,
                1,
                1
            ],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('language');
    }


}
