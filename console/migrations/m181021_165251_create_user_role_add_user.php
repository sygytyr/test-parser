<?php

use yii\db\Migration;
use yii\rbac\Item;

/**
 * Class m181021_165251_create_user_role_add_user
 */
class m181021_165251_create_user_role_add_user extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->batchInsert('{{%auth_item}}', ['name', 'type', 'created_at', 'updated_at'], [
            [
                '/*',
                Item::TYPE_PERMISSION,
                time(),
                time(),
            ]
        ]);

        $this->batchInsert('{{%auth_item}}', ['name', 'type', 'created_at', 'updated_at'], [
            [
                'AdminAccess',
                Item::TYPE_ROLE,
                time(),
                time(),
            ]
        ]);

        $this->batchInsert('{{%auth_item_child}}', ['parent', 'child'], [
            [
                'AdminAccess',
                '/*',

            ]
        ]);

        $this->batchInsert('{{%user}}', ['id', 'username', 'auth_key', 'password_hash', 'email', 'status','created_at', 'updated_at'], [
            [
                1,
                'admin',
                Yii::$app->security->generateRandomString(),
                Yii::$app->security->generatePasswordHash('123456'),
                'sygytyr@gmail.com',
                \backend\modules\rbac\models\User::STATUS_ACTIVE,
                time(),
                time(),

            ]
        ]);
        $this->batchInsert('{{%auth_assignment}}', ['item_name', 'user_id', 'created_at'], [
            [
                'AdminAccess',
                1,
                time(),
            ]
        ]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('{{%auth_item}}', ['name' => '/*']);
    }


}
