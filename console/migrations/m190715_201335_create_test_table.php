<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%test}}`.
 */
class m190715_201335_create_test_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        /**id - taken from api services*/
        $this->createTable('{{%test}}', [
            'id' => $this->string()->unique(),
            'internal_id' => $this->integer()->notNull(),
            'last_modify'=>$this->integer()->null(),
            'regulator'=>$this->string()->notNull(),
            'created_at' => $this->integer()->null(),
            'updated_at' => $this->integer()->null()

        ],'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');
        $this->addPrimaryKey('pk-test-id', '{{%test}}', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%test}}');
    }
}
