<?php

use yii\db\Migration;

/**
 * Handles the creation of table `language`.
 */
class m181021_182507_create_language_table extends Migration
{
    public function safeUp()
    {
        $this->createTable('language', [
            'id' => $this->primaryKey(),
            'short_label' => $this->string(10)->notNull(),
            'label' => $this->string(255)->notNull(),
            'locale' => $this->string(20)->notNull(),
            'default' => $this->integer(1)->defaultValue(0),
            'active' => $this->integer(1)->defaultValue(1),
            'sort_order' => $this->integer(11)->defaultValue(0)
        ]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

        $this->dropTable('language');
    }
}
