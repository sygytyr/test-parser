<?php

use yii\db\Migration;

/**
 * Class m181021_164707_addBlameableBehavior
 */
class m181021_164707_addBlameableBehavior extends Migration
{
    public function up()
    {
        $this->addColumn('{{%imagemanager}}', 'createdBy', $this->integer(10)->unsigned()->null()->defaultValue(null));
        $this->addColumn('{{%imagemanager}}', 'modifiedBy', $this->integer(10)->unsigned()->null()->defaultValue(null));
    }

    public function down()
    {
        $this->dropColumn('{{%imagemanager}}', 'createdBy');
        $this->dropColumn('{{%imagemanager}}', 'modifiedBy');
    }
}
