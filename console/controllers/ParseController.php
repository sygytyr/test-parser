<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 15.07.2019
 * Time: 23:31
 */

namespace console\controllers;


use backend\models\Test;
use yii\console\Controller;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\httpclient\Client;
use yii\web\NotFoundHttpException;

class ParseController extends Controller
{
    CONST API_URL = 'http://api.ias.brdo.com.ua/v1_1/inspections?apiKey=360d858edaac8313a73d237f340138c097ab6304';

    /**
     * @param int $limit
     * @return string
     */
    public function actionIndex($limit = 100) {

        $oldModelCount = $countExistsModel = Test::find()->count();
        if($countExistsModel > $limit){
            echo 'Parse all '.$limit. ' rows data';
            return '';
        }
        $page = 1;

        while ($countExistsModel < $limit )    {
            $data = $this->getData($page);
            $this->loadData($data, $countExistsModel, $limit);
            $page++;
        }
        echo 'Loaded '.($countExistsModel - $oldModelCount);
        return '';

    }

    /**
     * @param array $data
     * @param $countExistsModel
     * @param $limit
     */
    protected function loadData(array $data, &$countExistsModel, $limit)
    {
        foreach ($data as $row){
            if($countExistsModel >= $limit){
                break;
            }
            $model = new Test();
            $model->regulator = ArrayHelper::getValue($row, 'regulator');
            $model->id = ArrayHelper::getValue($row, 'id');
            $model->internal_id = ArrayHelper::getValue($row, 'internal_id');
            $model->last_modify = strtotime(ArrayHelper::getValue($row, 'last_modify'));
            if($model->validate() && $model->save()){
                $countExistsModel++;
            }

        }
    }

    /**
     * @return array|mixed
     * @throws NotFoundHttpException
     */
    protected function getData($page = 1)
    {
        $client = new Client();
        $responce = $client->get(self::API_URL.'&page='.$page)->send();

        if($responce->getHeaders()->get('http-code') != 200){
            throw new NotFoundHttpException('REST API is down');
        }
        $content = Json::decode($responce->getContent(), true);
        return  is_array($content) && isset($content['items']) ? $content['items'] : [];
    }
}